import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';


import { AppComponent } from './app.component';
import { FormsModule } from '@angular/forms';
import { AgmCoreModule } from '@agm/core';
import { ResaltarDirective } from './directives/resaltar.directive';
import { ContarClicksDirective } from './directives/contar-clicks.directive';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { Routes, RouterModule } from "@angular/router";
import { DetalleComponent } from './detalle/detalle.component';
import { LugaresComponent } from './lugares/lugares.component';
import { ContactoComponent } from './contacto/contacto.component';
import { CrearComponent } from './crear/crear.component';
import { LugaresService } from './services/lugares.service';
//firebase
import { AngularFireModule } from 'angularfire2';
import { AngularFireDatabaseModule, AngularFireDatabase } from 'angularfire2/database';
import { AngularFireAuthModule } from 'angularfire2/auth';
import { HttpModule } from "@angular/http";

const appRoutes: Routes = [
	{path:'/', component: LugaresComponent},
	{path:'lugares', component: LugaresComponent},
	{path:'contacto', component: ContactoComponent},
	{path:'crear', component: CrearComponent},
	{path:'detalle/:id', component: DetalleComponent}
];
export const firebaseConfig = {
  	apiKey: "AIzaSyDiKMtCzU2361ttHdXcXaw5dDjgvJkfuMg",
	authDomain: "platzisquare-cb3a0.firebaseapp.com",
	databaseURL: "https://platzisquare-cb3a0.firebaseio.com",
	storageBucket: "",
	messagingSenderId: "851090043472"
};

@NgModule({
  declarations: [
    AppComponent,
    ResaltarDirective,
    ContarClicksDirective,
    DetalleComponent,
    LugaresComponent,
    ContactoComponent,
    CrearComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    AgmCoreModule.forRoot({
      apiKey: 'AIzaSyCmWf_5Z2d62W6UGdk_AOTguoKB26sIhgw'
    }),
    BrowserAnimationsModule,
    RouterModule.forRoot(appRoutes),
    AngularFireModule.initializeApp(firebaseConfig),
    AngularFireDatabaseModule,
    AngularFireAuthModule,
    HttpModule
  ],
  providers: [LugaresService],
  bootstrap: [AppComponent]
})
export class AppModule { }
