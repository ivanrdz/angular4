import { Component } from '@angular/core';
import { LugaresService } from "../services/lugares.service";

@Component({
  selector: 'app-lugares',
  templateUrl: './lugares.component.html'
})
export class LugaresComponent { 
  title = 'Platzisquare'; 
  lat:number = 25.6427659;
  lng:number = 100.3244947;
  lugares = null;
  constructor(private lugaresService: LugaresService){
  	lugaresService.getLugares()
  		.valueChanges().subscribe(lugares => {
  			this.lugares = lugares;
  		});
  }
}